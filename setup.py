#!/usr/bin/env python

import sys, subprocess, os
from distutils.core import setup
from distutils.extension import Extension
import numpy as np

run_cython = '--run-cython' in sys.argv
if run_cython:
    sys.argv.remove('--run-cython')
    from Cython.Distutils import build_ext
    cmdclass = {'build_ext': build_ext}
else:
    cmdclass = {}

def get_version_from_git():
    curdir = os.path.dirname(os.path.abspath(__file__))
    try:
        p = subprocess.Popen(['git', 'describe', '--always'], cwd=curdir,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError:
        return

    if p.wait() != 0:
        return
    version = p.communicate()[0].strip()

    if version[0] == 'v':
        version = version[1:]

    try:
        p = subprocess.Popen(['git', 'diff', '--quiet'], cwd=curdir)
    except OSError:
        version += '-confused'  # This should never happen.
    else:
        if p.wait() == 1:
            version += '-dirty'
    return version

version = get_version_from_git()

# List of tuples (args, keywords) to be passed to Extension, possibly after
# replacing ".pyx" with ".c" if Cython is not to be used.
extensions = [(["cluster_tools.procutil", ["cluster_tools/procutil.pyx",
                                           "cluster_tools/readproc.c"]],
               {"depends" : ["cluster_tools/readproc.pxd",
                             "cluster_tools/readproc.h"]})
              ]


ext_modules = []
for args, keywords in extensions:
    if not run_cython:
        if 'language' in keywords:
            if keywords['language'] == 'c':
                ext = '.c'
            elif keywords['language'] == 'c++':
                ext = '.cpp'
            else:
                print >>sys.stderr, 'Unknown language'
                exit(1)
        else:
            ext = '.c'
        pyx_files = []
        cythonized_files = []
        sources = []
        for f in args[1]:
            if f[-4:] == '.pyx':
                pyx_files.append(f)
                f = f[:-4] + ext
                cythonized_files.append(f)
            sources.append(f)
        args[1] = sources

        try:
            cythonized_oldest = min(os.stat(f).st_mtime
                                    for f in cythonized_files)
        except OSError:
            msg = "{0} is missing. Run `./setup.py --run-cython build'."
            print >>sys.stderr, msg.format(f)
            exit(1)
        for f in pyx_files + keywords.get('depends', []):
            if os.stat(f).st_mtime > cythonized_oldest:
                msg = "{0} has been modified. " \
                "Run `./setup.py --run-cython build'."
                print >>sys.stderr, msg.format(f)
                exit(1)

    ext_modules.append(Extension(*args, **keywords))

include_dirs = [np.get_include()]

setup(name='cluster_tools',
      version=version,
      author='A. R. Akhmerov, C.W. Groth, M. Wimmer',
      #author_email='cwg@falma.de',
      description="A package for simplified cluster use",
      license="not to be distributed",
      packages=['cluster_tools'],
      scripts = ['scripts/cluster-free', 'scripts/cluster-run',
                 'scripts/cluster-daemon', 'scripts/kill-ipycluster',
	         'scripts/start-daemon', 'scripts/start-il-daemon',
                 'scripts/il-free'],
      cmdclass=cmdclass,
      ext_modules=ext_modules,
      include_dirs = include_dirs)
