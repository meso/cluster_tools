__all__ = ['IPyCluster']

import socket
import os
import tempfile
import subprocess
import time
import sys
import shutil
import atexit
import warnings
from threading import Lock
from IPython.parallel import Client
from cluster_run import schedule, SSH
from cluster_daemon import ClusterDaemonConnect
from mariscluster import isclusterip, isnovamaris


def _raise(error, ssh_mode):
    raisecmd = "raise " + error
    if ssh_mode:
        print raisecmd
        sys.exit(1)
    else:
        exec raisecmd


def _warn(warning, ssh_mode):
    warningcmd = "warnings.warn(" + warning + ")"
    if ssh_mode:
        print warningcmd
    else:
        exec warningcmd


def startcluster(enginehosts, controllerhost=None, num_engines=None,
                 ssh_mode=False):
    """Start an IPython cluster. Typically not used by end-users
    (use `IPyCluster` instead).

    Parameters
    ----------
    enginehosts: list of strings
        The machines on which to start the cluster engines.
    controllerhost: string or None
        The machine on which to start the controller. If None,
        the controller is started on the current machine.
        Defaults to None.
    num_engines: int or None
        Number of engines to start. If None, the maximum possible
        number of engines is started (currently 250). If `num_engines`
        is larger than the maximum, only the maximum possible engines
        are started (i.e. 250 right now). Defaults to None.
    ssh_mode: bool
        Whether `startcluster` is run via ssh (if True,
        exceptions and warnings are not raised, but printed
        to stdout). Defaults to False.
    """

    if num_engines > 250:
        _warn("'Only 250 engines are used, this is the maximum "
              "the novamaris cluster can support'", ssh_mode)
        num_engines = 250

    myip = socket.gethostbyname(socket.gethostname())

    if controllerhost is not None:
        try:
            controllerip = socket.gethostbyname(controllerhost)
        except socket.error:
            _raise("ValueError('Host " + controllerhost + " not known')",
                   ssh_mode)
    else:
        controllerip = myip

    if not isclusterip(controllerip):
        _raise("ValueError('The IPython controller must be run "
               "on the cluster')", ssh_mode)

    if not isclusterip(myip):
        _raise("RuntimeError('startcluster must be run on the cluster')",
               ssh_mode)

    # We are on the cluster, so we have direct access to all
    # machines as well as files

    ipythondir = os.path.expanduser('~/.ipython/')
    if not os.path.exists(ipythondir):
        os.mkdir(ipythondir)

    if not os.path.isdir(ipythondir):
        _raise("RuntimeError('Please remove the .ipython file from your "
                             "cluster home directory, it should actually be a "
                             "directory.')", ssh_mode)

    profiledir = tempfile.mkdtemp(prefix=ipythondir)

    # First launch the controller

    controllercmd = "nohup ipcontroller --ip=*"\
        " --log-to-file=True --debug --profile-dir='" + profiledir + \
        "' --TaskScheduler.hwm=0 &>/dev/null </dev/null &"

    if ssh_mode and isnovamaris(controllerip):
        rcode = subprocess.call(['/bin/bash', '-c',
                                 controllercmd])
    else:
        rcode = subprocess.call(SSH + [controllerip,
                                       '/bin/bash', '-c',
                                       "'" + controllercmd + "'"])

    if rcode != 0:
        _raise("RuntimeError('ipcontroller could not be launched, "
                             "ssh returned " + str(rcode) + "')", ssh_mode)


    # Now wait until the .json files are created
    client_json = profiledir + "/security/ipcontroller-client.json"
    engine_json = profiledir + "/security/ipcontroller-engine.json"
    wait = time.time() + 30
    while (not os.path.exists(client_json) or
           not os.path.exists(engine_json)):
        time.sleep(1)
        if time.time() > wait:
            _raise("RuntimeError('controller failed to start properly')",
                   ssh_mode)

    # Then launch all the machines
    if num_engines:
        n_passes = num_engines
    else:
        n_passes = 250      # our cluster does not support more

    if ssh_mode:
        progbar = False
    else:
        progbar = True

    pids = schedule(enginehosts,
                    ['ipengine --log-to-file=True --debug --profile-dir=' +
                     profiledir], n_passes,
                    oneshot=True, redirect=False, quiet=True,
                    progbar=progbar, pids=True)

    real_num = len(pids)

    if real_num == 0:
        _raise("RuntimeError('No engines could be launched, "
                            "cluster is full')", ssh_mode)
    elif num_engines and real_num < num_engines:
        _warn("'Requested " + str(num_engines) + " engines, "
              "only " + str(real_num) + " could be launched', "
              "RuntimeWarning", ssh_mode)

    # write the pids of the engines to file
    mach_pid = {}
    for mach, pid in pids:
        if mach in mach_pid:
            mach_pid[mach].append(pid)
        else:
            mach_pid[mach] = [pid]

    pidfile = open(profiledir + "/pid/engine.pids", "w")
    for mach, pids in mach_pid.iteritems():
        print >> pidfile, mach,
        for pid in pids:
            print >> pidfile, pid,
        print >> pidfile
    pidfile.close()

    return profiledir


def killcluster(pdir, ssh_mode=False):
    """Force kill the cluster associated with the profile directory
    `pdir`. Typically not used by end-users (use `IPyCluster`
    instead).

    Parameters
    ----------
    pdir: string
        The profile directory of the ipython cluster.
    ssh_mode: bool
        Whether `killcluster` is run via ssh (if True,
        exceptions and warnings are not raised, but printed
        to stdout). Defaults to False.
    """

    if os.path.exists(pdir + "/pid/ipcontroller.pid"):
        file = open(pdir + "/pid/ipcontroller.pid")
        pid = file.readline().rstrip()
        file.close()

        rcode1 = subprocess.call(["pkill", "-KILL", "-P", pid])
        rcode2 = subprocess.call(["kill", "-KILL", pid])

        if rcode1 or rcode2:
            _warn("'Could not kill the ipcontroller with pid %s'" % pid,
                  ssh_mode)

    if os.path.exists(pdir + "/pid/engine.pids"):
        file = open(pdir + "/pid/engine.pids")
        for line in file:
            words = line.split(" ")
            cmd = SSH + [words[0], "kill", "-KILL"]
            cmd.extend(words[1:])

            rcode = subprocess.call(cmd)

            if rcode:
                _warn("'Could not kill some cluster engines on %s'" %
                      words[0], ssh_mode)

        file.close()


def clusterload(pdir, ssh_mode=False):
    """Returns the actual load of the ipython cluster associated with
    the profile directory `pdir`. Typically not used by end-users (use
    `IPyCluster` instead).

    Parameters
    ----------
    pdir: string
        The profile directory of the ipython cluster.
    ssh_mode: bool
        Whether `clusterload` is run via ssh (if True,
        exceptions and warnings are not raised, but printed
        to stdout). Defaults to False.
    """

    # First, send query to all clusterdaemons on machines with engines
    pinglist = []
    if os.path.exists(pdir + "/pid/engine.pids"):
        file = open(pdir + "/pid/engine.pids")
        for line in file:
            words = line.split(" ")
            mach = words[0]
            pids = [int(pid) for pid in words[1:]]

            try:
                cdmn = ClusterDaemonConnect(mach)
                cdmn.soc.send_pyobj(("info", {'pids' : pids}))
                pinglist.append(cdmn)
            except:
                pass

        file.close()

    # Then, collect the results of the queries
    load = 0
    timeout = 10
    t1 = time.time()
    while(len(pinglist) and time.time() - t1 < timeout):
        to_be_removed = []
        for i, cdmn in enumerate(pinglist):
            if cdmn.soc.poll(timeout=10):
                answer = cdmn.soc.recv_pyobj()
                load += answer['my_procs']
                to_be_removed.append(i)

        to_be_removed.sort(reverse=True)
        for i in to_be_removed:
                del pinglist[i]

    return load


class IPyCluster:
    """Class managing an IPython cluster.
    """

    def __init__(self, enginehosts, controllerhost=None, num_engines=None):

        self.profiledir = None
        self.remoteprofiledir = None

        myip = socket.gethostbyname(socket.gethostname())

        if isclusterip(myip):
            self.profiledir = startcluster(enginehosts, controllerhost,
                                      num_engines)

            self._client = Client(self.profiledir +
                                  "/security/ipcontroller-client.json")

        else:
            # first make a local profile directory
            ipythondir = os.path.expanduser('~/.ipython/')
            if not os.path.exists(ipythondir):
                os.mkdir(ipythondir)

            if not os.path.isdir(ipythondir):
                raise RuntimeError("Please remove the .ipython file from "
                                   "your local home directory, it should "
                                   "actually be a directory")

            self.profiledir = tempfile.mkdtemp(prefix=ipythondir)

            # Now start the ipcluster remotely

            cmd = "from cluster_tools.ipycluster import startcluster; " \
                "profiledir = startcluster(("
            for host in enginehosts:
                cmd += "'" + host + "', "
            cmd += "), "
            if controllerhost is not None:
                cmd += "'" + str(controllerhost) + "'"
            else:
                cmd += "None"
            cmd += ", " + str(num_engines) + ", ssh_mode=True); "
            cmd += "print profiledir"

            p = subprocess.Popen(SSH + ['novamaris.lorentz.leidenuniv.nl',
                                        'python', '-c',
                                        "'" + cmd.replace("'", "'\\''") + "'"],
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)

            outputs = p.communicate()

            if p.returncode:
                for output in outputs[0].split("\n"):
                    if output[:5] == "raise":
                        exec output
                    else:
                        raise RuntimeError("Starting the cluster remotley "
                                           "failed:\n" + outputs[1])
            else:
                for i, output in enumerate(outputs[0].split("\n")):
                    if output[:13] == "warnings.warn":
                        exec output
                    else:
                        self.remoteprofiledir = output
                        client_json = self.remoteprofiledir + \
                            "/security/ipcontroller-client.json"
                        break

            # now copy the json file to the local machine
            rcode = subprocess.call(["scp", "-q",
                                     "novamaris.lorentz.leidenuniv.nl:" +
                                     client_json, self.profiledir])

            if rcode:
                raise RuntimeError("Failed to copy the client's json "
                                   "file to the local machine.")

            # for some reason, the current (May 2015) cluster installation
            # has the local (10.0.0.1) IP address in the json file.
            # This needs to be replaced by the external IP
            rcode = subprocess.call(["sed", "-i",
                                     "s/10\\.0\\.0\\.1/132\\.229\\.226\\.3/",
                                     self.profiledir +
                                     "/ipcontroller-client.json"])

            if rcode:
                raise RuntimeError("Failed to write proper IP address to "
                                   "json file " +  self.profiledir +
                                  "/ipcontroller-client.json .")

            self._client = Client(self.profiledir +
                                  "/ipcontroller-client.json")

        self._load_balanced_view = self._client.load_balanced_view()
        self.lock = Lock()

        atexit.register(self.shutdown)


    def shutdown(self, friendly=False):
        """Shutdown the ipython cluster. After shutdown, the ipython
        cluster cannot be used any more.

        Parameters
        ----------
        friendly : bool
            If True, the shutdown is initiated via the IPython.Parallel
            functionality. This works mostly, but if engines loose connection
            they might not be stopped. If False, the engines and controller
            are killed via the shell. This is a tiny bit slower, but safer.
            Defaults to False.
        """

        if friendly:
            if self._client is not None:
                with self.lock:
                    self._client.shutdown(hub=True)
        else:
            if self.remoteprofiledir:
                cmd = "from cluster_tools.ipycluster import killcluster;"
                cmd += "killcluster('%s', True)" % self.remoteprofiledir

                subprocess.call(SSH + ['novamaris.lorentz.leidenuniv.nl',
                                  'python', '-c',
                                  "'" + cmd.replace("'", "'\\''") + "'" ])
            elif self.profiledir:
                killcluster(self.profiledir)

        self._client = None

        if self.profiledir:
            shutil.rmtree(self.profiledir)
            self.profiledir = None

        if self.remoteprofiledir:
            subprocess.call(SSH + ['novamaris.lorentz.leidenuniv.nl',
                             'rm', '-rf', self.remoteprofiledir])
            self.remoteprofiledir = None

    def load(self):
        """Gives the load of the ipython cluster (1: cluster is fully
        loaded, 0: cluster not working at all)
        """

        if self.remoteprofiledir:
            cmd = "from cluster_tools.ipycluster import clusterload;"
            cmd += "load = clusterload('%s', True);" % self.remoteprofiledir
            cmd += "print load"

            p = subprocess.Popen(SSH + ['novamaris.lorentz.leidenuniv.nl',
                                  'python', '-c',
                                  "'" + cmd.replace("'", "'\\''") + "'" ],
                                 stdout=subprocess.PIPE)

            cload = float(p.communicate()[0].split("\n")[0])
        else:
            cload = clusterload(self.profiledir)

        nengines = len(self._client.ids)
        if nengines:
            return cload / nengines
        else:
            return 0

    def apply_async(self, func, *args, **kwargs):
        with self.lock:
            result = self._load_balanced_view.apply_async(func, *args,
                                                          **kwargs)
        return result

    def abort(self, msg_ids):
        with self.lock:
            result = self._client.abort(msg_ids)
        return result

    def spin(self):
        with self.lock:
            self._client.spin()
