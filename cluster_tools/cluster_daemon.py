from procutil import current_procs
import os
import socket
import subprocess
import time
import signal
from collections import namedtuple
import zmq

from logging import getLogger, Formatter, DEBUG
from logging.handlers import RotatingFileHandler

LOAD_GRACE = 0.1
BUSY_THRESHOLD = 0.3
RESERVATION_VALID = 300
POLL_TIME = 5

active_proc = namedtuple('active_proc', ['procs', 'mem'])
reservation = namedtuple('reserved', ['procs', 'mem', 'time'])


class ClusterDaemon:
    def __init__(self):
        self.pid = os.getpid()
        self.host = socket.gethostbyname(socket.gethostname())

        # set up the 0mq socket

        ctx = zmq.Context()
        self.soc = ctx.socket(zmq.REP)
        port = self.soc.bind_to_random_port("tcp://" + self.host)

        if os.path.exists("/clusterdata/python/clust.d"):
            # on cluster
            self.portfile = "/clusterdata/python/clust.d/" + \
                self.host + ".port"
            self.logfile = "/tmp/cluster-daemon.log"
            factor = 1
        elif  os.path.exists("/data/misc/wimmer/python/clust.d"):
            # on some other lorentz machine
            self.portfile = "/data/misc/wimmer/python/clust.d/" + \
                self.host + ".port"
            self.logfile = "/data/misc/wimmer/python/clust.d/" + \
                self.host + ".log"
            factor = 0.8 # keep some cores free on the lorentz machines
        else:
            raise RuntimeError("The cluster-daemon works only"
                               "on the cluster or lorentz machines")

        file_ = open(self.portfile, "w")
        os.chmod(self.portfile, 0664)
        print >> file_, port
        file_.close()

        # clean-up on kill
        def signal_handler(signal, frame):
            self.cleanup()
            exit()

        signal.signal(signal.SIGTERM, signal_handler)

        # find number of CPU's and memory

        cmd = "".join(("grep ^processor /proc/cpuinfo | wc -l; ",
                       "cat /proc/meminfo | grep MemTotal "
                       "| awk '{print $2, $3 }'"))

        p = subprocess.Popen(["/bin/bash", "-c", cmd], stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        outputs = p.communicate()[0].split()

        if len(outputs) != 3:
            raise ValueError("Unexpected output for cpus and memory")

        units = {'kB': 1024, 'mB': 1024 ** 2, 'gB': 1024 ** 3}
        self.max_load = int(int(outputs[0]) * factor)
        self.max_mem = int(outputs[1]) * units[outputs[2]]

        # prepare logging
        self.logger = getLogger('cluster-daemon')
        handler = RotatingFileHandler(self.logfile,
                                      maxBytes=2000000,
                                      backupCount=0)
        try:
            # the log-file might belong to another user
            os.chmod(self.logfile, 0664)
        except:
            pass

        handler.setFormatter(Formatter(fmt='%(asctime)s %(message)s'))
        self.logger.addHandler(handler)
        self.logger.setLevel(DEBUG)

        # prepare data structures

        self.reservations = {}
        self.res_num = 0
        self.active_processes = {}

        self.old_procs = self.procs = None
        self.update()

    def update(self):
        """
        """
        self.old_procs = self.procs
        self.procs = current_procs()
        self.loads = {}

        self.mem = 0
        self.load = 0


        # for logging
        active_pids = []
        active_load = 0
        real_load = 0

        for pid, proc in self.procs.items():

            if self.old_procs is not None and pid in self.old_procs:
                self.loads[pid] = ((proc.cputime -
                                    self.old_procs[pid].cputime) /
                                   (proc.curtime -
                                    self.old_procs[pid].curtime))
            else:
                self.loads[pid] = (proc.cputime /
                                   (proc.curtime - proc.start_time))


            if pid in self.active_processes:
                # active, registered processes always count at least
                # with the registered cpu time and memory

                effective_load = max(self.active_processes[pid].procs,
                                     self.loads[pid])
                effective_mem = max(self.active_processes[pid].mem,
                                    proc.rss)

                # for logging
                active_pids.append(pid)
                active_load += effective_load
                real_load += self.loads[pid]

                self.load += effective_load
                self.mem += effective_mem

            else:
                # anything above BUSY_THRESHOLD counts as at least 1 cpu
                if self.loads[pid] > BUSY_THRESHOLD:
                    self.load += max(self.loads[pid], 1.0)
                else:
                    self.load += self.loads[pid]

                self.mem += proc.rss

        if len(active_pids):
            self.logger.debug("Active pids " + str(active_pids) +
                              " are counted as load " + str(active_load) +
                              " but cause real load " + str(real_load))

        now = time.time()

        # check if reservations are still valid

        # Note: 2.7 has .viewitems() which would make everything much nicer
        to_be_removed = []
        for key, val in self.reservations.items():
            if val.time < now:
                to_be_removed.append(key)
        for key in to_be_removed:
            del self.reservations[key]


        self.logger.debug("Current load (without reservations) " +
                          str(self.load))

        # add the reservations
        self.load += sum([res.procs for res in self.reservations.values()])
        self.mem += sum([res.mem for res in self.reservations.values()])

        self.logger.debug("Tracked load (with reservations) " +
                          str(self.load))

        # remove non-existent pids from active_processes
        to_be_removed = []
        for pid in self.active_processes:
            if not pid in self.loads:
                to_be_removed.append(pid)
        for pid in to_be_removed:
            self.logger.debug("Removing pid " + str(pid) +
                              " from active processes")
            del self.active_processes[pid]

    def max_jobs(self, procs_per_job=1, mem_per_job=None):
        """
        """
        if procs_per_job > 0:
            njob_proc = int((self.max_load - self.load + LOAD_GRACE) /
                            procs_per_job)
        else:
            return 0

        if mem_per_job is not None and mem_per_job > 0:
            njob_mem = int((self.max_mem - self.mem) / mem_per_job)
            return min(njob_proc, njob_mem)
        else:
            return njob_proc

    def reserve(self, procs, mem=None):
        procs = max(procs, 0)
        if mem is None or mem < 0:
            mem = 0

        self.logger.debug("Reservation number " + str(self.res_num ) +
                          " with load=" + str(procs) + " and mem=" +
                          str(mem))

        self.reservations[self.res_num] = \
            reservation(procs=procs, mem=mem,
                        time=time.time() + RESERVATION_VALID)
        self.res_num += 1

        self.load += procs
        if mem is not None and mem > 0:
            self.mem += mem

        return self.res_num - 1


    def register(self, pids, procs_per_job=1, mem_per_job=None,
                 reservation=None):

        if procs_per_job is None or procs_per_job < 0:
            procs_per_job = 1
        if mem_per_job is None or mem_per_job < 0:
            mem_per_job = 0

        self.logger.debug("Registering pids " + str(pids) +
                          " with procs_per_job " + str(procs_per_job))
        for pid in pids:
            self.active_processes[pid] = active_proc(procs_per_job,
                                                     mem_per_job)

        if reservation in self.reservations:
            self.logger.debug("Removing reservation " + str(reservation))
            del self.reservations[reservation]


    def main_loop(self):

        while True:
            # Remember, we are a daemon and should not fail
            try:
                self.update()
                if self.soc.poll(POLL_TIME * 1000):
                    msg = self.soc.recv_pyobj()
                    self.logger.debug("Received message " + str(msg))
                else:
                    continue

                try:
                    query = msg[0]
                    options = msg[1]

                    if query == 'query':
                        self.action_query(options)
                    elif query == 'register':
                        self.action_register(options)
                    elif query == 'info':
                        self.action_info(options)
                    elif query == "shutdown":
                        self.cleanup()
                        exit()
                    else:
                        self.soc.send_pyobj({'error': 'unknown query'},
                                            zmq.NOBLOCK)

                except Exception as e:
                    self.soc.send_pyobj({'error': 'input error ' +
                                                  + '(python error: ' +
                                                  + str(e) + ')'},
                                        zmq.NOBLOCK)
                    continue
            except KeyboardInterrupt:
                self.cleanup()
                exit()
            except SystemExit:
                exit()
            except:
                print "Why did I come here?"
                continue


    def action_info(self, options):
        if 'pids' in options:
            # also return individual information about the processes in 'pids'
            pids = options['pids']

            my_load = 0
            my_mem = 0

            for pid in pids:
                if pid in self.loads:
                    my_load += self.loads[pid]
                if pid in self.procs:
                    my_mem += self.procs[pid].rss

            self.soc.send_pyobj({'total_procs': self.max_load,
                                 'used_procs': self.load,
                                 'my_procs': my_load,
                                 'total_mem': self.max_mem,
                                 'used_mem': self.mem,
                                 'my_mem': my_mem})
        else:
            self.soc.send_pyobj({'total_procs': self.max_load,
                                 'used_procs': self.load,
                                 'total_mem': self.max_mem,
                                 'used_mem': self.mem})

    def action_query(self, options):
        procs_per_job = options['procs_per_job']
        if 'mem_per_job' in options:
            mem_per_job = options['mem_per_job']
        else:
            mem_per_job = 0

        num_jobs = self.max_jobs(procs_per_job, mem_per_job)

        if 'want' in options:
            num_jobs = min(options['want'], num_jobs)

        if num_jobs > 0:
            res_num = self.reserve(num_jobs * procs_per_job,
                                   num_jobs * mem_per_job)

            self.soc.send_pyobj({'num_jobs': num_jobs,
                                 'res_num': res_num},
                                zmq.NOBLOCK)
        else:
            self.soc.send_pyobj({'num_jobs': 0},
                                zmq.NOBLOCK)


    def action_register(self, options):

        pids = options['pids']
        res_num = options['res_num']
        if 'procs_per_job' in options:
            procs_per_job = options['procs_per_job']
        else:
            procs_per_job = 1
        if 'mem_per_job' in options:
            mem_per_job = options['mem_per_job']
        else:
            mem_per_job = 0

        self.register(pids, procs_per_job,
                      mem_per_job, reservation=res_num)

        self.soc.send_pyobj({'success' : True},
                            zmq.NOBLOCK)


    def cleanup(self):
        print "cleanup"
        os.remove(self.portfile)


class ClusterDaemonConnect(object):
    def __init__(self, machine, quiet=False):
        self.host = socket.gethostbyname(machine)
        self.soc = None

        # figure out the port of the cluster-daemon
        if os.path.exists("/clusterdata/python/clust.d"):
            # on cluster
            portfile = "/clusterdata/python/clust.d/" + \
                self.host + ".port"
        elif  os.path.exists("/data/misc/wimmer/python/clust.d"):
            # on some other lorentz machine
            portfile = "/data/misc/wimmer/python/clust.d/" + \
                self.host + ".port"
        else:
            raise RuntimeError("connections to the cluster-daemon "
                               "work only on the cluster or lorentz machines")

        try:
            file = open(portfile)
            port = int(file.readline())
            file.close()
        except IOError:
            if not quiet:
                print "No cluster-daemon on machine", machine
            return
        except ValueError:
            if not quiet:
                print ".port-file for machine", machine, "seems damaged"
            return

        self.soc = zmq.Context().socket(zmq.REQ)
        self.soc.connect("tcp://" + self.host  + ":" + str(port))
        self.soc.setsockopt(zmq.LINGER, 0)


def main():
    dmn = ClusterDaemon()
    dmn.main_loop()


if __name__ == "__main__":
    main()
