"""Convenience module for our lorentz machines"""

def il():
    """Convenience function to create lists of lorentz machines
    """

    return ['bega', 'patinir', 'raedecker', 'corneille', 'isenbrant',
            'zilcken', 'berchem', 'constant', 'leck', 'rembrandt',
            'potter', 'vangogh', 'vermeer', 'bosch', 'hobbema']
