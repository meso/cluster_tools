from __future__ import division
import itertools
import cPickle
import os
import gzip
import numpy as np
import numpy.lib.recfunctions as rfn
import textwrap
from scipy.stats.morestats import bayes_mvs
from collections import namedtuple
try:
    import matplotlib
    from matplotlib.backends.backend_agg import FigureCanvasAgg
    import matplotlib.figure
    _mpl_enabled = True
except ImportError:
    _mpl_enabled = False

realpath = lambda path: os.path.abspath(os.path.expanduser(path))


class SplitData(namedtuple('SplitDataTuple',
                           ['arrays', 'parameters', 'constants'])):
    """Sets of data for different parameters (and constants).
    """

    def thermal_average(self, t, energy_name=None, spacing=None):
        """Wrapper function to array_thermal_average."""
        new_arrays = [array_thermal_average(ar, t, energy_name, spacing)
                      for ar in self.arrays]
        return SplitData(new_arrays, self.parameters, self.constants)

    def subset(self, selectfun):
        """Select the subset of data where selectfun(parameters) is `True`."""
        new_arrays = []
        new_params = []
        for ar, params in zip(self.arrays, self.parameters):
            if selectfun(params):
                new_arrays.append(ar)
                new_params.append(params)

        new_params_array = np.array(new_params, dtype=self.parameters.dtype)
        value_lists = [(arg, np.unique(new_params_array[arg])) for arg
                        in new_params_array.dtype.names]
        new_constants = [(value[0], value[1][0]) for value in value_lists
                          if len(value[1]) == 1]
        new_constants.extend(self.constants)
        drop_params = [value[0] for value in value_lists if len(value[1]) == 1]
        new_params_array = rfn.drop_fields(new_params_array, drop_params)

        return SplitData(new_arrays, new_params_array, new_constants)


def load_raw_data(*filenames):
    """Load pickled data from one or more files.

    Parameters
    ----------
    filenames : strings
        List of file names of the files with data.

    Returns
    -------
    data : list of dictionaries
      Unpickled data. There are no sanity checks, so the function actually
      just reads any pickled data, not just the output of `job`.

    Notes
    -----
    Only files containing exclusively pickled data are read without errors.
    """
    data = []
    for name in filenames:
        n = realpath(name)
        f = gzip.open(n, 'rb') if name[-3:] == '.gz' else open(n, 'r')
        while True:
            try:
                data.append(cPickle.load(f))
            except EOFError:
                break
        f.close()
    return data


def load_data(*filenames):
    """Load data as one or more annotated numpy arrays.

    A single array is returned per git version, module, and calculation type,
    i.e. set of functions that are called. Extra variables, such as date,
    username, time/memory usage are not added to the array.


    Parameters
    ----------
    filenames : strings
        List of file names of the files with data.

    Returns
    -------
    array_data : numpy structured array or an array-valued dict
      An array containing all the data, or a dictionary with version, module
      name and function list as keys, and structured arrays as values.

    Notes
    -----
    Only files containing exclusively pickled data are read without errors.
    """

    def entries():
        for name in filenames:
            n = realpath(name)
            f = gzip.open(n, 'rb') if name[-3:] == '.gz' else open(n, 'rb')
            while True:
                try:
                    yield cPickle.load(f)
                except EOFError:
                    break
            f.close()

    def tuple_to_dict(tuple_):
        return dict([('f' + str(i), j) for i, j in enumerate(tuple_)])

    def flatten_dict(dict_):
        result = {}
        for k, v in dict_.iteritems():
            if isinstance(v, (tuple, list)):
                v = tuple_to_dict(v)
            if isinstance(v, dict):
                v = flatten_dict(v)
                result.update(dict([(k + '.' + kk, vv) for kk, vv in
                              v.iteritems()]))
            else:
                result[k] = v
        return result

    result = {}
    for d in entries():
        # Get the program type (version, function names), remove technical
        # info.
        v = d.pop('_version')
        m = d.pop('_module')
        del d['_user'], d['_date'], d['_peak_rss'], d['_time']
        keys = tuple(sorted(d.keys()))
        ind = (v, m) + keys

        d = flatten_dict(d)

        if ind in result:
            for k, v in d.iteritems():
                result[ind][k].append(v)
        else:
            result[ind] = {}
            for k, v in d.iteritems():
                result[ind][k] = [v]

    for key, value in result.iteritems():

        r = rfn.merge_arrays([np.array(i) for i in value.itervalues()],
                             usemask=False, flatten=True)
        r.dtype.names = value.keys()
        result[key] = r
    if len(result) == 1:
        result = result.values()[0]
    return result


def rename_fields(data, rules):
    """Assign different names for fields in data.

    Mostly used in order to be able to write `result['G']` instead of
    `result['local_ns_conductance._out']`.

    Parameters
    ----------
    data : array or a dictionary with array-valued items
      The data, which has to be renamed.
    rules : dictionary of strings
      Renaming rules in form of `{'old_name1': 'new_name1'}`.

    Returns
    -------
    new_data : array or a dictionary with array-valued items
      Same data with different names.
    """
    if isinstance(data, dict):
        result = {}
        for k, v in data.iteritems():
            try:
                k = rules[k]
            except KeyError:
                pass
            result[k] = rfn.rename_fields(v, rules)
    else:
        result = rfn.rename_fields(data, rules)
    return result


def split_data(data_array, variables, drop=[]):
    """Split array data into a set of smaller arrays.

    We are typically interested in manipulating low-dimensional functions
    (e.g. conductance as a function of voltage and magnetic field). On the
    other hand, we have a lot of extra parameters, describing geometry,
    system shape, etc. This function return a list of arrays, each one only
    containing variables of interest, and a list of parameters characterizing
    each array.

    Parameters
    ----------
    data_array : numpy structured array
        Array with annotated fields, as produced by `array_from_dict`.
    variables : list of strings
        List of field names not used for splitting the data.
    drop : list of strings
        List of variables that should be dropped from the result

    Returns
    -------
    result : namedtuple('arrays', 'parameters', 'constants')
    result.arrays : list of numpy arrays
        List of numpy arrays with data corresponding to a given set of
        parameters.
    result.parameters : list of dictionaries
        List of parameters that are different across different arrays
    result.constants : dictionary
        Dictionary with parameters that are the same for every array.
    """
    d = data_array
    ign = set(variables)
    args = list(set(i for i in d.dtype.names) - ign - set(drop))
    ign = variables
    ign_data = d[ign]
    value_lists = [(arg, np.unique(d[arg])) for arg in args]
    constants = [(value[0], value[1][0]) for value in value_lists
                 if len(value[1]) == 1]
    var_args = [value[0] for value in value_lists if len(value[1]) > 1]
    if len(var_args) == 0:
        return SplitData([ign_data], np.zeros((1, 0)), constants)
    var_data = d[var_args]
    labels, indices = np.unique(var_data, return_inverse=True)
    result = [ign_data[indices == i] for i in range(len(labels))]
    return SplitData(result, labels, constants)


def _mv(array, alpha=0.68):
    if not np.isrealobj(array):
        raise ValueError('Only real-valued arrays are allowed.')
    ar = np.asarray(array, dtype=float)
    if np.allclose(array, array[0], rtol=1e-7, atol=1e-10):
        return array[0], 0., 0., 0.
    warning_settings = np.seterr(invalid='ignore')
    result = bayes_mvs(ar, alpha)[:2]
    np.seterr(**warning_settings)
    return result[0][0], (result[0][1][1] - result[0][1][0]) / 2, \
           result[1][0], (result[1][1][1] - result[1][1][0]) / 2


def mean_variance(data_array, variables, ignore, alpha=0.68):
    """Calculate statistics of variables.

    Calculate average and variance of certain variables and the corresponding
    confidence intervals (calculated via scipy.stats.morestats.bayes_mvs) with
    all the array variables except for some fixed.

    Parameters
    ----------
    data_array : numpy structured array
        Array with annotated fields, as produced by `array_from_dict`.
    variables : list of strings
        List of field names statistics of which is calculated.
    ignore : list of strings
        List of field names providing randomness or irrelevant for consequent
        calculation. These are just discarded in the result.
    alpha : double
        Confidence level with allowed values between 0 and 1. Default is 0.68,
        which corresponds to one standard deviation.

    Returns
    -------
    result : numpy structured array
        An array with annotated fields. For each variable in variables new
        fields are created with '.mean', '.mean_err', '.var', '.var_err'
        appended after its name which hold the corresponding values.
    """
    r, l, c = split_data(data_array, variables, ignore)
    new_labels = sum(([name + i for i in ('.mean', '.mean_err',
                      '.var', '.var_err')] for name in variables), [])
    c_names = [i[0] for i in c]
    n = len(r)
    mv = lambda ar: sum((_mv(ar[i], alpha) for i in variables), ())
    mv_res = np.array([mv(r[i]) for i in range(n)],
                      dtype=[(label, '<f8') for label in new_labels])
    res = [l, mv_res] + ([data_array[c_names][:n]] if c_names else [])
    return rfn.merge_arrays(res, usemask=False, flatten=True)


def add_columns(data, columns, labels):
    """Add columns to an data array and rename them.

    This is a convenience function based on numpy.lib.recfunctions

    Parameters
    ----------
    data : np.ndarray
      The data array, to which the columns should be added.
    columns : np.ndarray
      Columns to add.
    label : list of strings
      Names of the columns to add.
    """
    columns.dtype.names = labels
    return rfn.merge_arrays([data, columns], flatten=True, usemask=False)


def thermal_average(energies, values, t, spacing=None):
    """Convolve values with a derivative of a Fermi function.

    Parameters
    ----------
    energies : array-like of reals
      Array of energies at which the values are taken
    values : array-like
      Array of values to which the thermal_average has to be applied.
    t : double
      Temperature (measured in the same units as energies).
    spacing : double or None
      Distance between neighboring energies at which the thermal average is
      evaluated. When `spacing` is None, it is set to `t / 3`. Note that
      smaller spacings do not really make sense, since the output is smooth
      on the scale of temperature.

    Returns
    -------
    e_t : numpy.ndarray of doubles
        Array of energies at which the average was taken.
    v_t : numpy.ndarray of doubles
        Array of thermally averaged values evaluated at energies `e_t`.
    """
    if spacing is None:
        spacing = t / 3
    sort = np.argsort(energies)
    e = np.array(energies[sort])
    e = (e[1:] + e[: -1]) / 2
    w = e[1:] - e[:-1]
    e_r = np.arange(e[0], e[-1], spacing)
    c = np.array(values[sort])
    c = (c[1:] + c[: -1]) / 2
    c_r = np.zeros(e_r.shape)
    for i, en in enumerate(e_r):
        indices = slice(np.searchsorted(e, en - 6 * t),
                        np.searchsorted(e, en + 6 * t))
        e_tmp = np.exp((e[indices] - en) / t)
        e_tmp = (1 / t) / (e_tmp + 2 + 1 / e_tmp)
        c_r[i] = np.sum(e_tmp * c[indices] * w[indices]) / np.sum(e_tmp *
                                                                  w[indices])
    return e_r, c_r


def array_thermal_average(data, t, energy_name=None, spacing=None, cutoff=6.):
    """Perform thermal average on a structured numpy array.

    Parameters
    ----------
    data : 1D numpy structured array
        Data, to which the thermal average has to be applied.
    energy_name : string or None
        Name of the field in data, which stores values of energy. If `None`,
        the first field name is taken.
    t : double
        Temperature (measured in the same units as energies).
    spacing : double or None
        Distance between neighboring energies at which the thermal average is
        evaluated. When `spacing` is None, it is set to `t / 3`. Note that
        smaller spacings do not really make sense, since the output is smooth
        on the scale of temperature.
    cutoff : float
        Width of the interval used for determining the average, measured in
        units of `t`.

    Returns
    -------
    ouput_data : numpy structured array
        `output_data.dtype` is the same as that of `data`. The number of
        entries in `output_data` is set by the available energy range and the
        spacing.
    """
    if spacing is None:
        spacing = t / 3
    cutoff *= t
    if energy_name is None:
        energy_name = data.dtype.names[0]
    energies = data[energy_name]
    sort = np.argsort(energies)
    e = np.array(energies[sort])
    data2 = data[sort]
    a = np.empty((len(data) - 1,), dtype=data.dtype)
    b = np.empty((len(data) - 1,), dtype=data.dtype)
    e_d = np.diff(e)
    for f_name in data2.dtype.names:
        d_f = data2[f_name]
        a_f = a[f_name]
        b_f = b[f_name]
        a_f[:] = np.diff(d_f) / e_d
        b_f[:] = (e[1:] * d_f[: -1] - e[: -1] * d_f[1:]) / e_d
    e_r = np.arange(e[0], e[-1], spacing)
    output_data = np.empty((len(e_r),), dtype=data.dtype)
    output_data[energy_name] = e_r
    indices = np.empty((len(e_r), 2), 'int')
    e_tmp = []
    l = len(e)
    # Precalculate convolutions of line segments with a Fermi function
    # derivative.
    for i, en in enumerate(e_r):
        indices[i] = (max(np.searchsorted(e, en - cutoff) - 1, 0),
                    min(np.searchsorted(e, en + cutoff) + 1, l))
        et = (e[indices[i, 0]: indices[i, 1]] - en) / t
        tmp = np.exp(et)
        # Conditional evaluation is required for numerical stability.
        tmp2 = np.where(et > 0, 1 / (1 + 1 / tmp), tmp / (1 + tmp))
        tot = tmp2[-1] - tmp2[0]
        tmp3 = et * tmp2 - np.where(et > 0, et + np.log(1 + 1 / tmp),
                                    np.log(1 + tmp))
        e_tmp.append((np.diff(tmp2), np.diff(tmp3) * t, tot))

    for f_name in output_data.dtype.names:
        a_f, b_f = a[f_name], b[f_name]
        if f_name != energy_name:
            c_r = output_data[f_name]
            for i, en in enumerate(e_r):
                index = slice(indices[i, 0], indices[i, 1] - 1)
                a_i, b_i = a_f[index], b_f[index]
                b_fac, a_fac, weight = e_tmp[i]
                c_r[i] = np.sum((b_i + en * a_i) * b_fac + a_i * a_fac) / \
                                weight

    return output_data


def plot_2d_data(data, labels=None, x=None, y=None, errorbars=False,
                 constants={}, ax=None):
    """Create a figure from array data.

    Create a `matplotlib.pyplot.figure` containing a list of 2D curves
    with a legend. The input structure is similar to the output structure
    of split_data.

    Parameters
    ----------
    data : list of numpy arrays or `SplitData`
        Data, containing curves to plot. Length of data must equal length
        of labels.
    labels : numpy array or `SplitData` or None
        Parameter values characterizing every curve. Used to generate the
        plot legend. Ignored if None.
    x : string or None
        Name of the variable used for x-axis. If x is None, the first
        unused variable from the data array is used.
    y : string or None
        Name(s) of the variable used for y-axis. If multiple variables are
        given it has to be in the format `'variable1, variable2'`.
        If y is None, the first unused variable from the data array is used
        (so the second one, if the first one is used for x).
    errorbars: bool
        Show y-errors. Errors are assumed to be present in the data array under
        `'variablename'+'_err'`.
    constants : list of tuples (name, value) or `SplitData`
        List with values of all the other parameters (used to create the
        figure title)
    ax : `matplotlib.axes.Axes` instance or `None`
        If `ax` is not `None`, no new figure is created, but the plot is done
        within the existing Axes `ax`.

    Returns
    -------
    fig : matplotlib figure if `ax` is not set, else None
        Figure with the result, only returned if no `axis` is set.
        The figure is fully modifiable, and `matplotlib.pyplot.show()` will
        show the figure.
    """
    if not _mpl_enabled:
        raise RuntimeError('matplotlib is not installed.')

    if isinstance(data, SplitData):
        data = data.arrays

    if isinstance(labels, SplitData):
        labels = labels.parameters

    if isinstance(constants, SplitData):
        constants = constants.constants

    return_fig = False
    if ax is None:
        return_fig = True
        fig = matplotlib.figure.Figure()
        ax = fig.add_subplot(111)
    names = list(data[0].dtype.names)
    if y is not None:
        ylist = y.replace(' ', '').split(',')
        for y in ylist:
            del names[names.index(y)]
    if x is None:
        x = names[0]
    del names[names.index(x)]
    if y is None:
        ylist = [names[0]]
    for d in data:
        d[:] = d[np.argsort(d[x])]
    if errorbars:
        [[ax.errorbar(d[x], d[y], yerr=d[y+'_err'])
            for d in data] for y in ylist]
    else:
        [[ax.plot(d[x], d[y]) for d in data] for y in ylist]
    if labels is not None and len(labels)>1:
        var_names = list(labels[0].dtype.names)
        if len(ylist) > 1:
            leg_texts = []
            for y in ylist:
                leg_texts.append([', '.join((y + ', ' + i + '=' + str(j)
                             for i, j in itertools.izip(var_names, l)))
                             for l in labels])
            leg_texts = sum(leg_texts, [])
        else:
            leg_texts = [', '.join((i + '=' + str(j) for i, j in
                                itertools.izip(var_names, l)))
                         for l in labels]
        leg = ax.legend(leg_texts, 'upper right')
        rect = leg.get_frame()
        rect.set_facecolor(np.array([float(248) / float(255)] * 3))
        rect.set_linewidth(0.0)
    ax.grid(False)
    ax.set_xlabel(x)
    ax.set_ylabel(', '.join((y for y in ylist)))
    ax.set_title(', '.join((i[0] + '=' + str(i[1]) for i in constants)))
    ax.set_title(textwrap.fill(ax.title.get_text(), width=70));

    if return_fig:
        return fig

def plot_3d_data_colormap(data, x, y, z, xvals=None, yvals=None, vmin=None,
                          vmax=None, cmap=None, constants={}, ax=None,
                          tickcolor='w'):
    """Create a colormap figure from array data.

    Create a `matplotlib.pyplot.figure` containing a colormap a scalar
    function of to variables. The input structure is similar to the output
    structure of split_data.

    Parameters
    ----------
    data : list of numpy arrays or `SplitData`
        Data, containing the data to plot. Length of data must equal length
        of labels.
    x : string or None
        Name of the variable used for x-axis. If x is None, the first
        unused variable from the data array is used.
    y : string or None
        Name of the variable used for y-axis. If y is None, the first
        unused variable from the data array is used. (so the second one, if
        the first one is used for x)
    z : string or None
        Name of the variable which is mapped to a color scale. If z is None,
        the first unused variable from the data array is used. (so the second
        /third one, if the first/second one is used for x and or y).
    xvals : list of floats [x0, x1, ...] or None
        List of (evenly spaced) x-values for which z is ploted. If xvals is
        given, the list of x_i in data['x'] need not be complete. If xvals is
        None, unique(data['x']) is used which must be complete and evenly
        spaced.
    yvals : list of floats [y0, y1, ...] or None
        Same as xvals, only for the y-values.
    vmin : float or None
        Maximum value (upper cutoff) of the range of z which is mapped to
        the color scale.
    vmax : float or None
        Minimum value (lower cutoff) of the range of z which is mapped to
        the color scale.
    cmap : Colormap or None
        The matplotlib.colors.Colormap instance, eg. cm.jet, used for the
        color scale of the map. If None the default one is used.
    constants : list of tuples (name, value) or `SplitData`
        List with values of all the other parameters (used to create the
        figure title)
    ax : `matplotlib.axes.Axes` instance or `None`
        If `ax` is not `None`, no new figure is created, but the plot is done
        within the existing Axes `ax`.
    tickcolor : any matplotlib color
        Color of the x- and y-ticks.

    Returns
    -------
    fig : matplotlib figure if `ax` is not set, else None
        Figure with the result, only returned if no `axis` is set.
        The figure is fully modifiable, and `matplotlib.pyplot.show()` will
        show the figure.
    """
    if not _mpl_enabled:
        raise RuntimeError('matplotlib is not installed.')

    if isinstance(data, SplitData):
        data = data.arrays[0]
    if isinstance(constants, SplitData):
        constants = constants.constants

    names = list(data[0].dtype.names)
    if x is None:
        x = names[0]
    del names[names.index(x)]
    if y is None:
        y = names[0]
    del names[names.index(y)]
    if z is None:
        z = names[0]
    del names[names.index(z)]

    if xvals is None:
        xvals = np.sort(np.unique(data[x]))
    if yvals is None:
        yvals = np.sort(np.unique(data[y]))

    Z = np.zeros((len(yvals), len(xvals)))
    for an in np.nditer(data):
        xn, yn, zn = an[x], an[y], an[z]
        i = np.where(xvals==xn)[0][0]
        j = np.where(yvals==yn)[0][0]
        Z[j][i] = zn

    return_fig = False
    if ax is None:
        return_fig = True
        fig = matplotlib.figure.Figure()
        ax = fig.add_subplot(111)

    extent = [xvals[0], xvals[-1], yvals[0], yvals[-1]]
    extend = 'neither'
    if vmax is not None:
            extend = 'max'
    if vmin is not None:
        if vmin != 0:
            extend = 'both' if extend == 'max' else 'min'
    im = ax.imshow(Z, vmin=vmin, vmax=vmax, extent=extent, aspect='auto',
                   origin='lower', cmap=cmap, interpolation='none')
    ax.get_figure().colorbar(im, ax=ax, extend=extend)
    ax.set_xlabel(x)
    ax.set_ylabel(y)
    ax.figure.axes[1].set_ylabel(z)
    ax.set_title(', '.join((i[0] + '=' + str(i[1]) for i in constants)))
    ax.set_title(textwrap.fill(ax.title.get_text(), width=70));
    ax.tick_params(color=tickcolor, length=7, width=1.5)

    if return_fig:
        return fig

def output_fig(fig, output_mode='auto', fname=None, savefile_opts=None):
    """Output a matplotlib figure using a given output mode.

    Parameters
    ----------
    fig : matplotlib.figure.Figure instance
        The figure to be output.
    output_mode : string
        The output mode that has to be used. Can be one of the following:
        'pyplot' : attach the figure to pyplot, with the same behavior as if
        pyplot.plot was called to create this figure.
        'ipython' : attach a `FigureCanvasAgg` to the figure and return it.
        'return' : return the figure.
        'file' : save the figure into a file.
        'auto' : if fname is given, save to a file, else if pyplot
        is imported, attach to pyplot, otherwise just return. See also the
        notes below.
    fname : string
        The name of the file to save.
    savefile_opts : (list, dict) or None
        args and kwargs passed to `print_figure` of `matplotlib`

    Notes
    -----
    For IPython with inline plotting, automatic mode selects 'return', since
    there is a better way to show a figure by just calling `display(figure)`.
    """
    if not _mpl_enabled:
        raise RuntimeError('matplotlib is not installed.')
    if output_mode == 'auto':
        if fname is not None:
            output_mode = 'file'
        else:
            try:
                if matplotlib.pyplot.get_backend() != \
                     'module://IPython.zmq.pylab.backend_inline':
                    output_mode = 'pyplot'
                else:
                    output_mode = 'ipython'
            except AttributeError:
                output_mode = 'return'

    if output_mode == 'pyplot':
        try:
            fake_fig = matplotlib.pyplot.figure()
        except AttributeError:
            raise RuntimeError('matplotlib.pyplot has to be imported first.')
        fake_fig.canvas.figure = fig
        fig.canvas = fake_fig.canvas
        return fig
    elif output_mode == 'ipython':
        canvas = FigureCanvasAgg(fig)
        fig.canvas = canvas
        return fig
    elif output_mode == 'return':
        return fig
    else:
        canvas = FigureCanvasAgg(fig)
        if savefile_opts is None:
            savefile_opts = ([], {})
        canvas.print_figure(output_mode, *savefile_opts[0],
                            **savefile_opts[1])
