#include <sys/types.h>
#include <dirent.h>

struct PHandle {
  DIR *dir;
  long tics_per_sec, page_size;
  unsigned long boottime;
};

struct PHandle *openproc(void);
void closeproc(struct PHandle *);
int readproc(struct PHandle *,
	     int *, int *, double *,
	     double *, double *, long *);

