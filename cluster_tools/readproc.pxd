cdef extern from "readproc.h":
    struct PHandle:
        pass

    PHandle *openproc()
    void closeproc(PHandle *)
    int readproc(PHandle *, int *, int *, double *, double *, double *, long *)

