"""Convenience module for our maris cluster"""

cluster_ips = ['132.229.226.3'] + ['10.0.0.' + str(i) for i in xrange(74)]


def isclusterip(ip):
    return ip in cluster_ips


def isnovamaris(ip):
    return ip == cluster_ips[0]


def maris(machines=None, exceptions=None):
    """Convenience function to create lists of cluster machines
    """

    if machines is None:
        machines = xrange(2, 74)

    if exceptions is None:
        exceptions = ()

    mlist = []
    for mach in machines:
        if mach not in exceptions:
            num = str(mach)
            zeros = 3 - len(num)
            mlist.append('maris' + zeros * '0' + num)

    return mlist
