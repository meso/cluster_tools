from __future__ import division
import inspect
import getpass
import time
import datetime
import resource
import threading
import Queue
import subprocess
import itertools
import sys
import os
import imp
import cPickle
import gzip
from functools import reduce
from IPython.parallel import TimeoutError
import numpy as np
import numpy.lib.recfunctions as rfn
try:
    import cluster_run
except ImportError:
    pass
import utils

prod = itertools.product
izip_longest = itertools.izip_longest
reverse_enumerate = lambda l: itertools.izip(xrange(len(l) - 1, -1, -1),
                                             reversed(l))
realpath = lambda path: os.path.abspath(os.path.expanduser(path))


class F(object):
    """A function wrapper for functions used in intermediate calculations."""
    def __init__(self, name, **kwargs):
        self.name = name
        self.kwargs = kwargs

    def __repr__(self):
        return "F('{0}', **{1})".format(self.name, self.kwargs)


class R(object):
    """A function wrapper for function, with output to be stored."""
    def __init__(self, name, **kwargs):
        self.name = name
        self.kwargs = kwargs

    def __repr__(self):
        return "R('{0}', **{1})".format(self.name, self.kwargs)


class I(object):
    """A wrapper for iterator input to exec_job_ipython."""
    def __init__(self, iterator):
        self.iter_ = iter(iterator)

    def __getitem__(self, item):
        return self, item


class A(object):
    """A descriptor for adaptive evaluation.\

    Parameters
    ----------
    interval : 2-tuple-like of floats
        Lower and upper bounds for the argument of adaptive evaluation.
    output_vars : list of lists or string or None
        Specification of the output variables to be used for adaptive
        evaluation. If None all the outputs are used. List of lists has to
        have the format `[['func1', 'key1'], ['func2', 'key1', 1]], while the
        string has to have format `'func1.key1, func2.key2.f1'`. This example
        would correspond to making
        `(result['func1']['_out']['key1'], result['func2']['_out']['key2'][1])`
        the output of adaptive calculation, where `result` is returned by
        `exec_job`. This result must always be a tuple of numerical types.
    """
    def __init__(self, interval, output_vars=None, init_points=15,
                precision_goal=.05, max_dissection=5, n_split=1):
        self.interval = interval
        self.init_points = init_points
        self.sq_x_scale = (interval[0] - interval[1]) ** 2
        self.min_sq_x_scale = .5 ** (max_dissection * 2) / init_points ** 2
        self.precision_goal_sq = precision_goal ** 2
        self.n_split = n_split

        def fi_to_num(str_):
            if str_[0] == 'f' and str_[1:].isdigit():
                return int(str_[1:])
            else:
                return str_

        if isinstance(output_vars, list) or output_vars is None:
            out = output_vars
        elif isinstance(output_vars, str):
            out = output_vars.replace(' ', '')
            out = [[fi_to_num(j) for j in i.split('.')]
                                for i in out.split(',')]
        if out is not None:
            [i.insert(1, '_out') for i in out]
        self.out = out

    def get_result(self, result):
        """Get output variables from AsyncResult, return input if not ready."""
        if not hasattr(result, 'msg_ids'):
            return result
        try:
            value = result.get(0)
            tmp = [reduce(lambda i, j: i[j], k, value) for k in self.out]
            output = []
            for i in tmp:
                try:
                    output.extend(i)
                except TypeError:
                    output.append(i)
            return output
        except TimeoutError:
            return result


class IPyResult(object):
    """An (incomplete) result of calculation done with ipython cluster."""
    def __init__(self):
        self.data = []
        self.failed = []
        self._running_jobs = set()
        self.queue = Queue.Queue()
        self.finished = threading.Event()
        self.lock = threading.Lock()

    def _pop_queue(self):
        while not self.queue.empty():
            self._running_jobs = self._running_jobs | self.queue.get()

    def update(self):
        """Put the result of the successfully completed jobs into data."""
        # TODO: Return meaningful progress estimate.
        self._pop_queue()
        with self.lock:
            ready = set(i for i in self._running_jobs
                        if i.ready() and i.successful())
            self.data += [i.get(0) for i in ready]
            self._running_jobs -= ready
            failed = set(i for i in self._running_jobs
                         if i.ready() and not i.successful())
            if failed:
                self.failed += list(failed)
                self._running_jobs -= failed
                raise RuntimeError('There were failed jobs, they are added to '
                                   'the IPyResult.`failed` set.')

    def abort(self):
        """Abort unfinished calculations, update the result."""
        self.finished.set()
        self.thread.join()
        self._pop_queue()
        msg_ids = [i.msg_ids[0] for i in self._running_jobs]
        self.ipycluster.abort(msg_ids)
        self.ipycluster.spin()
        for i in self._running_jobs:
            while not i.wait(1):
                continue
        self.data += [i.get() for i in self._running_jobs if i.successful()]

    def dump(self, filename, method='raise'):
        """Save all the calculated data to file.

        Parameters
        ----------
        filename : string
            File where data has to be saved. If filename ends in '.gz', data
            is stored in gzipped format.
        method : {'raise', 'append', 'overwrite'}, optional
            Whether to raise an error if the file exists (default), append to
            the file, or overwrite it.
        """
        n = realpath(filename)
        if method == 'raise':
            if os.path.exists(n):
                raise RuntimeError('File already exists.')
            mode = 'wb'
        elif method == 'append':
            mode = 'ab'
        elif method == 'overwrite':
            mode = 'wb'
        else:
            raise ValueError('Illegal value of method.')
        f = gzip.open(n, mode) if filename[-3:] == '.gz' else open(n, mode)
        try:
            for i in self.data:
                cPickle.dump(i, f, protocol=-1)
        finally:
            f.close()

    def show(self):
        """Show the calculated data as a structured numpy array."""
        # Mostly copied from data.load_data

        if len(self.data) == 0:
            return []

        def tuple_to_dict(tuple_):
            return dict([('f' + str(i), j) for i, j in enumerate(tuple_)])

        def flatten_dict(dict_):
            result = {}
            for k, v in dict_.iteritems():
                if isinstance(v, (tuple, list)):
                    v = tuple_to_dict(v)
                if isinstance(v, dict):
                    v = flatten_dict(v)
                    result.update(dict([(k + '.' + kk, vv) for kk, vv in
                                v.iteritems()]))
                else:
                    result[k] = v
            return result

        result = {}
        for d in self.data:
            # Get the program type (version, function names), remove technical
            # info.
            d = flatten_dict(d)

            del d['_user'], d['_date'], d['_peak_rss'], \
                d['_time'], d['_version'], d['_module']

            if len(result) == 0:
                for k in d.iterkeys():
                    result[k] = []

            for k, v in d.iteritems():
                result[k].append(v)

        r = rfn.merge_arrays([np.array(i) for i in result.itervalues()],
                            usemask=False, flatten=True)
        r = rfn.rename_fields(r, dict([('f' + str(i), name) for i, name in
                                    enumerate(result.iterkeys())]))
        return r

    def completed(self):
        """Return if the calculation is completed."""
        if not self.finished.is_set():
            return False
        self._pop_queue()
        return all((i.ready() for i in self._running_jobs))

    def wait(self, timeout=None):
        """Wait for calculation to complete or timeout."""
        if timeout == 0:
            timeout = 1e-2  # Necessary to account for python overhead.
        if timeout is None or timeout < 0:
            while not self.finished.wait(1):
                continue
            self.thread.join()
            self._pop_queue()
            [j.wait() for j in self._running_jobs]
            self.update()
            return True
        else:
            end_time = time.time() + timeout
            while (end_time - time.time() > 1) and not self.finished.wait(1):
                continue
            if not self.finished.wait(end_time - time.time()):
                return False
            self.thread.join()
            self._pop_queue()
            for i in self._running_jobs:
                if not i.wait(end_time - time.time()):
                    return False
            self.update()
            return True

class Oracle(object):
    """Selects data to be calculated in an adaptive evaluation.\

    Parameters
    ----------
    a : instance of `A`
        Adaptive discriptor of the evaluation.
    """
    def __init__(self, a):
        # internal variables:
        self._xintervals = {}
        self._unrequested_x = set(a.interval)
        self._intervals = self._split_interval(a.interval, [None, None],
                                                        a.init_points)
        self._add_xintervals(self._intervals)
        self._a = a
        self._n_done = 0
        self._sq_scales = None
        self._min_vec = None
        self._max_vec = None
        self._refresh_min_max = 0

    # internal functions:
    def _sq_norm(self, origin, end, sq_scales):
        return sum((i - j) ** 2 / k for i, j, k in itertools.izip(origin, end,
                   sq_scales))

    def _max_min(self, points):
        point_ar = np.array(points)
        max_vec, min_vec = np.apply_along_axis(np.max, 0, point_ar), \
                           np.apply_along_axis(np.min, 0, point_ar)
        return max_vec, min_vec

    def _get_sq_scales(self):
        # sq_scale need not be refreshed every time
        self._refresh_min_max -=1
        if self._refresh_min_max < 0:
            y_data = [v[0] for v in self._intervals.itervalues()
                    if v[0] is not None]
            if self._max_vec is not None:
                y_data += [self._max_vec, self._min_vec]
            self._max_vec, self._min_vec = self._max_min(y_data)
            self._sq_scales = [max(i - j, 1e-4 * i, 1e-8) \
                    for i, j in itertools.izip(self._max_vec, self._min_vec)]
            self._refresh_min_max = 10
        return self._sq_scales

    def _split_interval(self, interval, value, n_points=1):
        points = np.linspace(interval[0], interval[1], n_points + 2)
        values = [value[0]] + [None for point in points[1: -1]] + [value[1]]
        #add new x-values to unrequested x:
        for x in points[1:-1]:
            self._unrequested_x.add(x)
        return dict(((points[i], points[i + 1]), [values[i], values[i + 1]])
                for i in xrange(len(points) - 1))

    def _add_xintervals(self, intervals):
        for k in intervals.iterkeys():
            if k[0] in self._xintervals:
                self._xintervals[k[0]].add(k)
            else:
                self._xintervals[k[0]] = {k}
            if k[1] in self._xintervals:
                self._xintervals[k[1]].add(k)
            else:
                self._xintervals[k[1]] = {k}

    # functions visible from the outside:
    def add_data(self, data):
        """Adds data to the internal intervals.

        Parameters:
        -----------
        data : list or set of tuples (float, list or array of floats) 
            [(x0, [y00, y01, ...]), (x1, [y10, y11, ..] ), ...]
            where (xi, [yij]) are data tuples x, y-array.
        """ 
        a = self._a
        # determine new intervals
        new_intervals = {}
        if self._n_done < max(a.init_points *.75, 7):
            # We're in the beginning -> split all finished
            for x, y in data:
                finished_intervals = []
                for k in self._xintervals[x]:
                    # find x and set y in internal intervals
                    #print k
                    v = self._intervals[k]
                    if k[0] == x:
                        index = 0
                    else:
                        index = 1
                    v[index] = y
                    self._n_done +=1
                    if v[(index+1)%2] is not None:
                        # interval is finished -> split it
                        new_intervals.update(
                                    self._split_interval(k, v, a.n_split))
                        finished_intervals.append(k)
                for k in finished_intervals:
                    del self._intervals[k]
                    if k[0] in self._xintervals:
                        self._xintervals[k[0]].discard(k)
                    if k[1] in self._xintervals:
                        self._xintervals[k[1]].discard(k)
                del self._xintervals[x]
            self._intervals.update(new_intervals)
            self._add_xintervals(new_intervals)
            return

        # Main loop, split only if not meeting precision goal
        for x, y in data:
            finished_intervals = []
            for k in self._xintervals[x]:
                # find x and set y in internal intervals
                v = self._intervals[k]
                if k[0] == x:
                    index = 0
                else:
                    index = 1
                self._intervals[k][index] = y
                v[index] = y
                sq_scales = self._get_sq_scales()
                if v[(index+1)%2] is not None:
                    # interval is finished -> split if precision is not met
                    dx = (k[0] - k[1]) ** 2 / a.sq_x_scale
                    dist = dx + self._sq_norm(v[0], v[1], sq_scales)
                    if dist > a.precision_goal_sq \
                                and dx > a.min_sq_x_scale:
                        new_intervals.update(
                                self._split_interval(k, v, a.n_split))
                    finished_intervals.append(k)
            for k in finished_intervals:
                del self._intervals[k]
                if k[0] in self._xintervals:
                    self._xintervals[k[0]].discard(k)
                if k[1] in self._xintervals:
                    self._xintervals[k[1]].discard(k)
            del self._xintervals[x]
        self._add_xintervals(new_intervals)
        self._intervals.update(new_intervals)

    def request_points(self):
        """Returns set of points to be calculated.
        
        Returns:
        --------
        points : set of floats
            Unique set of requested points which are needed/available before
            additional data is added).
        """
        points = self._unrequested_x
        self._unrequested_x = set()
        return points

def exec_job_list_ipython(ipycluster, module, jobs, ignore_version=False):
    """Execute a series of calculations using an ipython cluster client.

    This function allows for a flexible definition of the calculation to be
    performed and uses a somewhat involving syntax for defining the
    calculation.

    Parameters
    ----------
    ipycluster : cluster_tools.ipycluster.IPyCluster instance
        The ipython cluster on which the calculation has to be
        performed.
    module : string
        Path to the file of the module containing the functions
        to be executed.
    jobs : one or more tuples.
        The format of the tuples is almost identical to that of `job`
        argument of `exec_job`, so it is a list of `F` and `R` objects with
        names of the functions to be executed, and arguments to these
        functions. The values of the arguments define the sequence of
        calculations to be performed, with the format specified below.
    ignore_version : bool
        Whether to ignore the availability of git version or raise an error.
        Only set to True for testing purposes.

    Calculation format
    ------------------
    The most common use case of the calculation is a nested loop over several
    parameters. In order for definition of such a calculation to be easy,
    every value that is iterable except a string (and some special tuples) is
    interpreted as a loop over the values of the argument. So if one writes
    `energy = np.linspace(0, 1, 11)`, this mean that every calculation has to
    be repeated for 11 values of the variable `energy`.

    Sometimes we want to change several parameters simultaneously. This is
    achieved by using the iterator markup object `I(iterator)`. The
    iterator argument to `I` must return a list-like object or a single item.
    Then using the same I object for different arguments means that their
    values should be simultaneously taken from this iterator, as in the
    following code::

      loop = I([[0., 0.1], [.1, .2], [.3, .4]])
      F('sys', energy=loop[0], mu=loop[1])

    In this example the calculation will be repeated three times, with `energy`
    values `(0, .1, .3)`, and `mu` values `(.1, .2, .4)`.

    Finally, the most complicated calculation format is adaptive, marked by
    the `A` (adaptive) object. It allows to evaluate data until a given global
    precision goal is reached (lower bound on the error after averaging,
    smoothness of a curve, etc). Only a single instance of `A` can be used as
    an argument, although it can be given as a value to several variables.
    More details on how to use adaptive objects will follow later.

    Examples
    --------
    >>> exec_job_list_ipython(cluster, '~/src/project/quantum_dot.py',
    >>>                       [F('qdot', r=xrange(3, 20, 3), mu=[0., .2, .5]),
    >>>                        R('conductance', sys=F('qdot'),
    >>>                          e=A(-.1, .1))])
    """
    module = realpath(module)
    for j in jobs:
        if not isinstance(j, (F, R)):
            raise ValueError('Incorrect job description')

    # Prepare to unpack value lists into a job description.
    ixs = xrange(len(jobs))
    lengths = np.cumsum([0] + [len(j.kwargs) for j in jobs])
    slices = [slice(i, j) for i, j in zip(lengths[:-1], lengths[1:])]
    types = [type(j) for j in jobs]
    names = [j.name for j in jobs]
    keys = sum([j.kwargs.keys() for j in jobs], [])

    def command_from_list(list_):
        return [types[i](names[i],
                 **dict(zip(keys[slices[i]], list_[slices[i]]))) for i in ixs]

    # Generate value lists and coordinate unpacking.
    values = sum([j.kwargs.values() for j in jobs], [])
    iters, coords = [], []
    for i, val in enumerate(values):
        if isinstance(val, I):
            if val.iter_ not in iters:
                iters.append(val.iter_)
                coords.append([i])
            else:
                coords[iters.index(val.iter_)].append(i)
            continue
        if isinstance(val, tuple) and isinstance(val[0], I):
            if val[0].iter_ not in iters:
                iters.append(val[0].iter_)
                coords.append([(i, val[1])])
            else:
                coords[iters.index(val[0].iter_)].append((i, val[1]))
            continue
        if isinstance(i, str):
            continue
        try:
            iters.append(iter(val))
            coords.append([i])
        except TypeError:
            continue
    itervals = prod(*iters)

    def unpack_values(val):
        for i, v in zip(coords, val):
            # Catch the special case of I-objects returning a sequence.
            try:
                for j in i:
                    values[j[0]] = v[j[1]]
            except TypeError:
                for j in i:
                    values[j] = v
        return values

    commands = [command_from_list(unpack_values(val)) for val in itervals]
    res = IPyResult()

    launch = lambda job: ipycluster.apply_async(exec_job, module,
                                                'return', ignore_version, *job)

    # Function returning a submitted job from the adaptive variable value.
    def make_job(spec, point):
        spec[0][spec[1]].kwargs[spec[2]] = point
        return launch(spec[0])

    # Initialize the data for the adaptive jobs.
    adaptive_tasks = []
    for job in commands:
        # Figure out where A object sits, get it, and check if it is unique.
        addr = []
        for i, el in enumerate(job):
            for k, v in el.kwargs.iteritems():
                if isinstance(v, A):
                    addr.append((i, k, v))
        if len(addr) == 0:  # Non-adaptive job, just add to running jobs.
            res._running_jobs.add(launch(job))
            continue
        elif len(addr) > 1:  # Will be generalized later.
            raise(ValueError('Only one adaptive argument is allowed.'))
        else:
            func, key, a = addr[0]

        # If the adaptive variable does not have its output_vars set, set them.
        if a.out is None:
            a.out = [[r.name, '_out'] for r in job if isinstance(r, R)]

        # Launch initial sampling points.
        spec = job, func, key
        o = Oracle(a)
        launched = {make_job(spec, x) for x in o.request_points()}

        res._running_jobs = res._running_jobs | launched
        adaptive_tasks.append([launched, spec, a, o])

    # Helper functions to extract x and y for finished adaptive jobs:
    extract_x = lambda job, spec: job[spec[0][spec[1]].name][spec[2]]
    def extract_y(job, spec):
        y = []
        for name, out in a.out:
            res = job[name][out]
            if hasattr(res, '__iter__'):
                y.extend(res)
            else:
                y.append(res)
        return y

    # Convenience functions to run the adaptive loop.
    def loop_step(task):
        launched, spec, a, o = task
        if len(launched) == 0:
            return False
        # Update the calculations and sort them out if finished
        finished = {j for j in launched if j.ready()}
        launched -= finished

        # Submit to oracle finished and start new requests.
        o.add_data([(extract_x(j.get(0), spec), extract_y(j.get(0), spec)) 
                                                            for j in finished])
        new_jobs = {make_job(spec, x) for x in o.request_points()}
        launched |= new_jobs
        res.queue.put(new_jobs)

        return True

    def threaded_loop(tasks):
        try:
            with res.lock:
                tasks_done = [loop_step(task) for task in tasks]
            while any(tasks_done):
                for i in xrange(30):
                    if res.finished.is_set():
                        break
                    time.sleep(.1)

                if res.finished.is_set():
                    break
                with res.lock:
                    tasks_done = [loop_step(task) for task in tasks]
        finally:
            res.finished.set()

    res.thread = threading.Thread(target=threaded_loop, args=(adaptive_tasks,))
    res.thread.start()
    res.ipycluster = ipycluster
    return res


def exec_job(module, output_mode='print', ignore_version=False, *job):
    """Execute a sequence of functions from a module, and store the result.

    Define a single computational job consisting out of consequtive calls to a
    set of functions belonging to the same module. After the computation is
    performed, useful metrics are stored together with the result of
    calculation.

    Parameters
    ----------
    module : string
        Path to the module (with full filename) which provides all the
        necessary functions to execute.
    output_mode : string
        How the output should be provided. The allowed values are:
        - 'print' for dumping pickled output into stdout
        - 'print_raw' for printing the output as plaintext
        - 'return' for returning the result.
    ignore_version : bool
        Whether to ignore the availability of git version or raise an error.
        Only set to True for testing purposes.
    job : one or more instances of `F` or `R`
        Each of these instances describe a function to calculate. The format
        of each instance should be
        `F('function_name', arg1=value1, arg2=value2)`, which corresponds to a
        function call function_name(arg1=value1, arg2=value2). If an `R`
        object is used instead of `F`, the result of the calculation is kept
        in the output. `F` objects are used for intermediate calculations,
        which do not enter the final result. If value of one of the arguments
        is F('func') or R('func'), the result of previous calculation is used
        as the value of this argument.

        Note that functions are evaluated in the sequence in which they are
        provided, so only preceding functions may be called in this manner.
        If store is True, the output of the function is stored as a result.

    Examples
    --------
    >>> exec_job('~/src/project/quantum_dot.py', git_version='1.0', F('qdot',
    >>>          r=30, mu=0.), R('conductance', sys=F('qdot'), e=0))
    """
    if output_mode not in ('print', 'print_raw', 'return'):
        raise ValueError("output_mode should be one of 'print', "
                         "'print_raw', 'return'.")
    t = - time.time()
    launch_date = datetime.datetime.now()
    git_version = get_version_from_git(module)
    if git_version == 'unknown' and not ignore_version:
        raise RuntimeError('Unknown source code version.')
    module = realpath(module)
    m = imp.load_source('m', module)
    result = {}
    f_names = [f.name for f in job]

    # Determine which calculation results are to be saved for how long.
    keep = len(job) * [0]
    for i, fn in reverse_enumerate(f_names):
        try:
            if isinstance(job[i], R):
                keep[i] = float('inf')
        except IndexError:
            pass
        for value in job[i].kwargs.itervalues():
            if isinstance(value, (F, R)):
                j = f_names.index(value.name)
                if keep[j] == 0:
                    keep[j] = i

    # Catch some (but not all) causality errors.
    for i, j in enumerate(keep):
        if j <= i:
            raise ValueError('Impossible order of function evaluation.')

    # Call all the functions, deleting stored results as they expire.
    for i, f in enumerate(job):
        func = m.__dict__[f.name]
        ins = inspect.getargspec(func)
        args, values = ins.args, ins.defaults
        if values is None:
            values = []
        kwargs = dict(zip(args[-len(values):], values))
        kwargs.update(f.kwargs)
        kwargs_store = kwargs.copy()
        for key, value in kwargs.iteritems():
            if isinstance(value, (F, R)):
                name = value.name
                kwargs_store[key] = name
                kwargs[key] = result[name]['_out']
                if keep[f_names.index(name)] == i:
                    del result[name]['_out']
        kwargs_store['_out'] = func(**kwargs)
        result[f.name] = kwargs_store
        del kwargs

    t += time.time()
    result.update({'_date': launch_date,
                   '_user': getpass.getuser(), '_version': git_version,
                   '_time': t, '_peak_rss': get_peak_memory(),
                   '_module': module})

    if output_mode == 'print':
        cPickle.dump(result, sys.stdout, protocol=-1)
    elif output_mode == 'print_raw':
        print result
    elif output_mode == 'return':
        return result


def get_version_from_git(path):
    p = os.path
    code_dir = os.path.dirname(os.path.abspath(path))
    try:
        p = subprocess.Popen(['git', 'describe', '--always'], cwd=code_dir,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError:
        return 'unknown'

    if p.wait() != 0:
        return 'unknown'
    version = p.communicate()[0].strip()

    if version[0] == 'v':
        version = version[1:]

    try:
        p = subprocess.Popen(['git', 'diff', '--quiet'], cwd=code_dir)
    except OSError:
        version == 'unknown'  # This should never happen.
    else:
        if p.wait() == 1:
            version = 'unknown'
    return version


def get_peak_memory():
    return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss


def exec_job_list(machines, module, jobs, n_jobs=1, progbar=True,
                  mem_per_job=None, ignore_version=False):
    """Execute a series of jobs on maris cluster using cluster_run.

    This function is useful for running most of tasks on maris cluster. It
    expands loops over parameters, so it allows to define large sequences of
    jobs in a single go. It is designed to run from a terminal, so it doesn't
    support more advanced iteration types.

    Parameters
    ----------
    machines : list of integers or strings
        Names of the machines on which the jobs have to be executed.
        Maris machines can be specified with an integer.
    module : string
        Path to the file of the module containing the functions to be executed.
    jobs : tuple of F and R objects
        The format of the jobs is almost identical to that of `job`
        argument of `exec_job`. The only (very important) difference is that
        all the arguments that are iterable except for strings are looped over.
        This allows to vary a lot of parameters within a single call of this
        function.
    n_jobs : integers
        How many calculations should one process perform. This is useful for
        small tasks, in order to avoid cluster_run overhead.
    progbar: True or False
        Whether to display a bar showing the progress of starting jobs on the
        cluster. Defaults to `True`.
    mem_per_job: integer, string or None
        The requested amount of memory for a single job. Can be given
        as a number (interpreted as bytes), as a string ('1.2 gb'), or
        None if no memory requirements are specified. Defaults to None.
    ignore_version : bool
        Whether to ignore the availability of git version or raise an error.
        Only set to True for testing purposes.

    Examples
    --------
    >>> exec_job_list(range(48, 60), '~/src/project/quantum_dot.py',
    >>>               (F('qdot', r=xrange(3, 20, 3), mu=[0., .2, .5]),
    >>>                R('conductance', sys=F('qdot'), e=0)),
    >>>               n_jobs=5)
    """

    machines = ['maris' + (3 - len(str(i))) * '0' + str(i) 
                if isinstance(i, int) else i for i in
                machines]
    memory = utils.memory_to_bytes(mem_per_job)
    module = realpath(module)
    prefix = 'python \'' + os.path.abspath(__file__) + '\' '
    comm = '"exec_job(\'{0}\', \'print\', {2}, {1})"'.format(module, '{0}',
                                                             ignore_version)
    for j in jobs:
        if not isinstance(j, (F, R)):
            raise ValueError('Incorrect job description')
    lengths = np.cumsum([0] + [len(j.kwargs) for j in jobs])
    slices = [slice(i, j) for i, j in itertools.izip(lengths[:-1],
                                                     lengths[1:])]
    types = [type(j) for j in jobs]
    names = [j.name for j in jobs]
    keys = list(itertools.chain(*[j.kwargs.iterkeys() for j in jobs]))
    values = list(itertools.chain(*[j.kwargs.itervalues() for j in jobs]))
    itervalues = []
    for i in values:
        if isinstance(i, str):
            itervalues += [[i]]
        else:
            try:
                itervalues += [iter(i)]
            except TypeError:
                itervalues += [[i]]
    ixs = xrange(len(jobs))
    commands = [[types[i](names[i],
                 **dict(zip(keys[slices[i]], vals[slices[i]]))) for i in ixs]
                for vals in prod(*itervalues)]
    commands = [comm.format(', '.join([str(j) for j in i])) for i in commands]
    commands = [prefix + ' '.join(i) for i in izip_longest(fillvalue='',
                                             *([iter(commands)] * n_jobs))]

    cluster_run.schedule(machines, commands, header=False, quiet=True,
                         mem_per_job=memory)


if __name__ == '__main__':
    for command in sys.argv[1:]:
        exec(command)
